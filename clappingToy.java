
public class clappingToy {
	String face="O=";
	String facePrint="";
	String feetPrint="";
	double spring=0;
	boolean run=false;
	
	
	
	coilSpring clappingToy= new coilSpring(.5,true);
	

	//gets the run to see if true or false
public boolean isRun() {
		return run;
	}

	//sets run
	public void setRun(boolean run) {
		this.run = run;
	}

	//method creates the persons body and hands clapping and feet
	public  String createClap() {
	
	spring=Double.parseDouble(clappingToy.toString());
	//if run is true the mouth and feet are created
	//O= and O> represents the person clapping
	//LL represent the legs
	if (run==true) {
		for (double clappingCounter=spring; clappingCounter>0; clappingCounter=clappingCounter-1) {
	
			if (face=="O=") {
			feetPrint=feetPrint+"LL  ";
			facePrint=facePrint+"O=  ";
			face="O>";	
		}
			
		if (face=="O>") {
			feetPrint=feetPrint+"LL  ";
			facePrint=facePrint+"O>  ";
			face="O=";
			
		}
		
	}

	//if run is false nothing will be printed
	}else if (run==false) {
		feetPrint="";
		facePrint="";
	}
	return facePrint +"\n"+ feetPrint;
}

//method runs all the methods in clappingToy class
public clappingToy(boolean run) {
	super();
	this.run=run;

}
	
//returns the printed person clapping
@Override
public String toString() {
		return createClap()+" ";

}

}
