
	public class mouthToy {
		 
		String feet="M";
		String mouthPrint="";
		String feetPrint="";
		double spring=0;
		boolean run=false;
		
		
		
		coilSpring mouth= new coilSpring(8,true);
		
	
		//gets the run to see if true or false
	public boolean isRun() {
			return run;
		}

		//sets run
		public void setRun(boolean run) {
			this.run = run;
		}

		//method creates the mouth and feet and prints them until the spring reaches zero
	public  String createMouth() {
		
		spring=Double.parseDouble(mouth.toString());
		//if run is true the mouth and feet are created
		//< and > represents the mouth
		// M and W represent the mouth moving
		if (run==true) {
			for (double mouthCounter=spring; mouthCounter>0; mouthCounter=mouthCounter-1) {
			if (feet=="M") {
				feetPrint=feetPrint+"M  ";
				mouthPrint=mouthPrint+"<  ";
				feet="W";	
			}
				
			if (feet=="W") {
				feetPrint=feetPrint+"W  ";
				mouthPrint=mouthPrint+">  ";
				feet="M";
				
			}
			
		}

		//if run is false nothing will be printed
		}else if (run==false) {
			feetPrint="";
			mouthPrint="";
		}
		return mouthPrint +"\n"+ feetPrint;
	}
	
	//method runs all the methods in mouthToy class
public mouthToy(boolean run) {
		super();
		this.run=run;
	
	}
		
	//returns the printed mouth and feet to play class
@Override
	public String toString() {
			return createMouth()+" ";
	
	}

}




