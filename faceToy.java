
public class faceToy {
	String eyes="|o  o|";
	String eyesPrint="";
	String mouthPrint="";
	double spring=0;
	boolean run=false;
	
	
	
	coilSpring faceToy= new coilSpring(1.4,true);
	

	//gets the run to see if true or false
public boolean isRun() {
		return run;
	}

	//sets run
	public void setRun(boolean run) {
		this.run = run;
	}

	//method creates the mouth and feet and prints them until the spring reaches zero
public  String createFace() {
	
	spring=Double.parseDouble(faceToy.toString());
	//if run is true the mouth and feet are created
	//|o  o| and |o  -|represents the face blinking 
	// | ~~ | represent the mouth
	if (run==true) {
		for (double faceCounter=spring; faceCounter>0; faceCounter=faceCounter-1) {
		if (eyes=="|o  o|") {
			eyesPrint=eyesPrint+"|o  o| ";
			mouthPrint=mouthPrint+"| ~~ | ";
			eyes="|o  -|";	
		}
			
		if (eyes=="|o  -|") {
			eyesPrint=eyesPrint+"|o  -| ";
			mouthPrint=mouthPrint+"| ~~ | ";
			eyes="|o  o|";
			
		}
		
	}

	//if run is false nothing will be printed
	}else if (run==false) {
		eyesPrint="";
		mouthPrint="";
	}
	return eyesPrint +"\n"+ mouthPrint;
}

//method runs all the methods in faceToy class
public faceToy(boolean run) {
	super();
	this.run=run;

}
	
//returns the printed mouth and eyes to play class
@Override
public String toString() {
		return createFace()+" ";

}

}
