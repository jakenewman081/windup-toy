
public class truckToy {
		 
		String wheels="0  0";
		String truckBodyPrint="";
		String wheelsPrint="";
		double spring=0;
		boolean run=false;
		
		
		
		coilSpring truckToy= new coilSpring(4.6,true);
		
	
		//gets the run to see if true or false
	public boolean isRun() {
			return run;
		}

		//sets run
		public void setRun(boolean run) {
			this.run = run;
		}

		//method creates the truck body and tires and prints them until the spring reaches zero
	public  String createTruck() {
		
		spring=Double.parseDouble(truckToy.toString());
		//if run is true the mouth and feet are created
		//|__| represents the truck body
		// O  O and o  o represent the truck moving and tires squashing 
		if (run==true) {
			for (double truckCounter=spring; truckCounter>0; truckCounter=truckCounter-1) {
		
				if (wheels=="0  0") {
				wheelsPrint=wheelsPrint+"O  O  ";
				truckBodyPrint=truckBodyPrint+"|__|  ";
				wheels="o  o";	
			}
				
			if (wheels=="o  o") {
				wheelsPrint=wheelsPrint+"o  o  ";
				truckBodyPrint=truckBodyPrint+"|__|  ";
				wheels="0  0";
				
			}
			
		}

		//if run is false nothing will be printed
		}else if (run==false) {
			wheelsPrint="";
			truckBodyPrint="";
		}
		return truckBodyPrint +"\n"+ wheelsPrint;
	}
	
	//method runs all the methods in truckToy class
public truckToy(boolean run) {
		super();
		this.run=run;
	
	}
		
	//returns the printed truck body and tires to play class
@Override
	public String toString() {
			return createTruck()+" ";
	
	}
}


