
public class cyclopsToy {
	String face="(o)";
	String facePrint="";
	String feetPrint="";
	double spring=0;
	boolean run=false;
	
	
	
	coilSpring cyclopsToy= new coilSpring(1,true);
	

	//gets the run to see if true or false
public boolean isRun() {
		return run;
	}

	//sets run
	public void setRun(boolean run) {
		this.run = run;
	}

	//method creates the cyclops face and feet 
public  String createCyclops() {
	
	spring=Double.parseDouble(cyclopsToy.toString());
	//if run is true the mouth and feet are created
	//(o) and (-) represents the cyclops blinking
	// O  O and o  o represent the truck moving and tires squashing 
	if (run==true) {
		for (double truckCounter=spring; truckCounter>0; truckCounter=truckCounter-1) {
	
			if (face=="(o)") {
			feetPrint=feetPrint+"L L  ";
			facePrint=facePrint+"(o)  ";
			face="(-)";	
		}
			
		if (face=="(-)") {
			feetPrint=feetPrint+"L L  ";
			facePrint=facePrint+"(-)  ";
			face="(o)";
			
		}
		
	}

	//if run is false nothing will be printed
	}else if (run==false) {
		feetPrint="";
		facePrint="";
	}
	return facePrint +"\n"+ feetPrint;
}

//method runs all the methods in cyclopsToy class
public cyclopsToy(boolean run) {
	super();
	this.run=run;

}
	
//returns the printed cyclops face and feet
public String toString() {
		return createCyclops()+" ";

}

}
