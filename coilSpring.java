/**
 * 
 */

/**
 * @author newmanj87939
 *
 */
public class coilSpring {
	
	static double keyTurns=0;
	boolean lockPosition;
	static double springTightness=0;
	
	
	public double getKeyTurns() {
		return keyTurns;
	}
	
	public void setKeyTurns(double keyTurns) {
		this.keyTurns = keyTurns;
	}
	
	public boolean isLockPosition() {
		return lockPosition;
	}
	
	public void setLockPosition(boolean lockPosition) {
		this.lockPosition = lockPosition;
	}
	
	public double springTightness() {
		if (lockPosition==true && keyTurns<=10) {
			springTightness=(keyTurns*5);
		}else if (lockPosition==false) {
			springTightness=0;
		}
		
		return springTightness;
	}
	
	@Override
	public String toString() {
		return springTightness()+"";
		
	}
	
	public coilSpring(double keyTurns, boolean lockPosition) {
		super();
		this.keyTurns=keyTurns;
		this.lockPosition=lockPosition;
		
	}
}
		
		


